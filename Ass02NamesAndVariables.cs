using System;
					
public class Program
{
	public static void Main()
	{
	int numberOfCars = 100;
	double spaceInACar = 4.0;
	int numberOfDrivers = 30;
	int numberOfPassengers = 90;
	int carsNotDriven = numberOfCars - numberOfDrivers;
	int carsDriven = numberOfDrivers;
	double carpoolCapacity = numberOfCars * spaceInACar;
	double averagePassengersPerCar = numberOfPassengers / carsDriven;
	double numberOfTransporting = spaceInACar * numberOfDrivers;

	Console.WriteLine("There are " + numberOfCars +" available today.");
	Console.WriteLine("There are only " + numberOfDrivers + " available.");
	Console.WriteLine("There will be " + carsNotDriven + " empty cars today.");
	Console.WriteLine("We can transport " + numberOfTransporting + " people today.");
	Console.WriteLine("We have " + carpoolCapacity + " to carpool today.");
	Console.WriteLine("We need to put " + averagePassengersPerCar + " in each car."); 

	}
}